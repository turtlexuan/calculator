//
//  calculatorTests.m
//  calculatorTests
//
//  Created by Turtle Liu on 2020/4/8.
//  Copyright © 2020 Turtle Liu. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CalculatorViewModel.h"
//#import "CalculatorViewModel.m"

@interface calculatorTests : XCTestCase {
    CalculatorViewModel* viewmodel;
}

@end

@implementation calculatorTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    viewmodel = [[CalculatorViewModel alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    viewmodel = nil;
}

- (void)testEnterNumbers {
    [viewmodel enterNumber:@"1"];
    [viewmodel enterNumber:@"4"];
    [viewmodel enterNumber:@"2"];
    [viewmodel enterNumber:@"8"];
    
    XCTAssertEqualObjects(viewmodel.textToDisplay, @"1428");
}

- (void)testPlusAction {
    [viewmodel enterNumber:@"3"];
    [viewmodel enterNumber:@"9"];
    [viewmodel enterOperation:CalculatorOperationPlus];
    [viewmodel enterNumber:@"1"];
    [viewmodel equalAction];
    
    XCTAssertEqualObjects(viewmodel.textToDisplay, @"40");
}

- (void)testMinusAction {
    [viewmodel enterNumber:@"3"];
    [viewmodel enterNumber:@"9"];
    [viewmodel enterOperation:CalculatorOperationMinus];
    [viewmodel enterNumber:@"1"];
    [viewmodel equalAction];
    
    XCTAssertEqualObjects(viewmodel.textToDisplay, @"38");
}

- (void)testMultiplyAction {
    [viewmodel enterNumber:@"2"];
    [viewmodel enterNumber:@"0"];
    [viewmodel enterOperation:CalculatorOperationMultiply];
    [viewmodel enterNumber:@"3"];
    [viewmodel equalAction];
    
    XCTAssertEqualObjects(viewmodel.textToDisplay, @"60");
}

- (void)testDevideAction {
    [viewmodel enterNumber:@"2"];
    [viewmodel enterNumber:@"0"];
    [viewmodel enterOperation:CalculatorOperationDevide];
    [viewmodel enterNumber:@"4"];
    [viewmodel equalAction];
    
    XCTAssertEqualObjects(viewmodel.textToDisplay, @"5");
}

- (void)testMultiOperation {
    [viewmodel enterNumber:@"2"];
    [viewmodel enterNumber:@"0"];
    [viewmodel enterOperation:CalculatorOperationPlus];
    [viewmodel enterNumber:@"4"];
    [viewmodel enterOperation:CalculatorOperationMultiply];
    [viewmodel enterNumber:@"8"];
    [viewmodel equalAction];
    
    XCTAssertEqualObjects(viewmodel.textToDisplay, @"52");
}

- (void)testEnterNumberAfterEqualAction {
    [viewmodel enterNumber:@"2"];
    [viewmodel enterNumber:@"0"];
    [viewmodel enterOperation:CalculatorOperationPlus];
    [viewmodel enterNumber:@"4"];
    [viewmodel enterOperation:CalculatorOperationMultiply];
    [viewmodel enterNumber:@"8"];
    [viewmodel equalAction];
    [viewmodel enterNumber:@"6"];
    [viewmodel enterNumber:@"8"];
    
    XCTAssertEqualObjects(viewmodel.textToDisplay, @"68");
}

- (void)testContinueOperateAfterEqualAction {
    [viewmodel enterNumber:@"2"];
    [viewmodel enterNumber:@"0"];
    [viewmodel enterOperation:CalculatorOperationPlus];
    [viewmodel enterNumber:@"4"];
    [viewmodel enterOperation:CalculatorOperationMultiply];
    [viewmodel enterNumber:@"8"];
    [viewmodel equalAction];
    [viewmodel enterOperation:CalculatorOperationDevide];
    [viewmodel enterNumber:@"4"];
    [viewmodel equalAction];
    
    XCTAssertEqualObjects(viewmodel.textToDisplay, @"13");
}

- (void)testClearAction {
    [viewmodel enterNumber:@"2"];
    [viewmodel enterNumber:@"0"];
    [viewmodel enterOperation:CalculatorOperationPlus];
    [viewmodel enterNumber:@"4"];
    [viewmodel enterOperation:CalculatorOperationMultiply];
    [viewmodel enterNumber:@"8"];
    [viewmodel clearAction];
    
    XCTAssertEqualObjects(viewmodel.textToDisplay, @"0");
}

@end
