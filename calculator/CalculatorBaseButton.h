//
//  CalculatorBaseButton.h
//  calculator
//
//  Created by Turtle Liu on 2020/4/7.
//  Copyright © 2020 Turtle Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CalculatorBaseButton : UIButton

@end

NS_ASSUME_NONNULL_END
