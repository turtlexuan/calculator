//
//  CalculatorView.h
//  calculator
//
//  Created by Turtle Liu on 2020/4/6.
//  Copyright © 2020 Turtle Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import "CalculatorBaseButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface CalculatorView : UIView

@property (nonatomic) UILabel * displayLabel;

@property (nonatomic) CalculatorBaseButton * clearButton;
@property (nonatomic) CalculatorBaseButton * negtiveButton;
@property (nonatomic) CalculatorBaseButton * percentButton;

@property (nonatomic) CalculatorBaseButton * equalButton;
@property (nonatomic) CalculatorBaseButton * plusButton;
@property (nonatomic) CalculatorBaseButton * minusButton;
@property (nonatomic) CalculatorBaseButton * multiplyButton;
@property (nonatomic) CalculatorBaseButton * devideButton;

@property (nonatomic) CalculatorBaseButton * decimalButton;
@property (nonatomic) CalculatorBaseButton * zeroButton;
@property (nonatomic) CalculatorBaseButton * oneButton;
@property (nonatomic) CalculatorBaseButton * twoButton;
@property (nonatomic) CalculatorBaseButton * threeButton;
@property (nonatomic) CalculatorBaseButton * fourButton;
@property (nonatomic) CalculatorBaseButton * fiveButton;
@property (nonatomic) CalculatorBaseButton * sixButton;
@property (nonatomic) CalculatorBaseButton * sevenButton;
@property (nonatomic) CalculatorBaseButton * eightButton;
@property (nonatomic) CalculatorBaseButton * nineButton;

- (void)setButtonHighlight:(nullable UIButton *)button;

@end

NS_ASSUME_NONNULL_END
