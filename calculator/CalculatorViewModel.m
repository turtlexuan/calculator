//
//  CalculatorViewModel.m
//  calculator
//
//  Created by Turtle Liu on 2020/4/7.
//  Copyright © 2020 Turtle Liu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CalculatorViewModel.h"

@interface CalculatorViewModel ()

@property (nonatomic) double keepedNumber;
@property (nonatomic) double leftNumber;
@property (nonatomic) NSString* rightNumber;
@property (nonatomic) CalculatorOperation keepedOperation;
@property (nonatomic) CalculatorOperation currentOperation;

@end

@implementation CalculatorViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self clearAction];
    }
    return self;
}

- (void)enterNumber:(NSString *)number {
    
    if (self.currentOperation == CalculatorOperationEnd) {
        self.rightNumber = @"0";
        self.currentOperation = CalculatorOperationNone;
    }
    
    if ([number isEqual:@"0"] && [self.textToDisplay isEqual:@"0"]) {
        return;
    }
    
    if (self.textToDisplay.length >= 9) {
        return;
    }
    
    NSString* currentNumber;
    
    if ([number isEqual:@"-"]) {
        currentNumber = [[NSNumber numberWithDouble:[self.rightNumber doubleValue] * -1] stringValue];
    } else {
        if ([self.rightNumber isEqual:@"0"]) {
            currentNumber = number;
        } else if ([self.rightNumber isEqual:@"-0"]) {
            currentNumber = [NSString stringWithFormat:@"-%@", number];
        } else {
            currentNumber = [self.rightNumber stringByAppendingString:number];
        }
    }
    
    if ([number isEqual:@"."]) {
        if (![self.rightNumber containsString:@"."]) {
            currentNumber = [self.rightNumber stringByAppendingString:@"."];
        }
    }
    
    self.textToDisplay = [currentNumber copy];
    self.rightNumber = currentNumber;
}

- (void)enterOperation:(CalculatorOperation)operation {
    if ([self shouldKeepNumberWithNewOperator:operation]) {
        self.keepedOperation = self.currentOperation;
        self.keepedNumber = self.leftNumber;
        self.currentOperation = operation;
        self.leftNumber = [self.rightNumber doubleValue];
        self.rightNumber = @"0";
        return;
    }
    
    if (self.currentOperation != CalculatorOperationNone && self.currentOperation != CalculatorOperationEnd) {
        double result = [self calculateResultWithLeftNumber:self.leftNumber withRightNumber:[self.rightNumber doubleValue] withOperator:self.currentOperation];
        self.leftNumber = result;
        self.rightNumber = @"0";
        self.currentOperation = operation;
        [self doKeepedActionIfNeeded];
        return;
    }
    
    self.leftNumber = [self.rightNumber doubleValue];
    self.rightNumber = @"0";
    self.currentOperation = operation;
}

- (void)doKeepedActionIfNeeded {
    if (self.keepedOperation == CalculatorOperationNone) {
        NSDecimalNumber *dn = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf", self.leftNumber]];
        self.textToDisplay = [dn stringValue];
        return;
    }
    
    double result = [self calculateResultWithLeftNumber:self.keepedNumber withRightNumber:self.leftNumber withOperator:self.keepedOperation];
    self.keepedOperation = CalculatorOperationNone;
    self.keepedNumber = 0;
    self.leftNumber = result;
    self.rightNumber = @"0";
    self.textToDisplay = [[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf", result]] stringValue];
}

- (void)equalAction {
    if (self.currentOperation == CalculatorOperationNone) {
        return;
    }
    double result = [self calculateResultWithLeftNumber:self.leftNumber withRightNumber:[self.rightNumber doubleValue] withOperator:self.currentOperation];
    self.leftNumber = 0;
    self.rightNumber = [[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf", result]] stringValue];
    if (self.keepedOperation != CalculatorOperationNone) {
        result = [self calculateResultWithLeftNumber:self.keepedNumber withRightNumber:result withOperator:self.keepedOperation];
        self.keepedOperation = CalculatorOperationNone;
        self.keepedNumber = 0;
        self.leftNumber = 0;
        self.rightNumber = [[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf", result]] stringValue];
    }
    self.currentOperation = CalculatorOperationEnd;
    self.textToDisplay = [[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%lf", result]] stringValue];
}

- (void)clearAction {
    self.keepedNumber = 0;
    self.leftNumber = 0;
    self.rightNumber = @"0";
    self.keepedOperation = CalculatorOperationNone;
    self.currentOperation = CalculatorOperationNone;
    self.textToDisplay = @"0";
}

- (double)calculateResultWithLeftNumber:(double)leftNumber withRightNumber:(double)rightNumber withOperator:(CalculatorOperation)operator {
    switch (operator) {
        case CalculatorOperationPlus:
            return leftNumber + rightNumber;
            break;
        case CalculatorOperationMinus:
            return leftNumber - rightNumber;
            break;
        case CalculatorOperationMultiply:
            return leftNumber * rightNumber;
            break;
        case CalculatorOperationDevide:
            return leftNumber / rightNumber;
            break;
        default:
            return 0;
            break;
    }
}

- (BOOL)shouldKeepNumberWithNewOperator:(CalculatorOperation)operator {
    if (operator == CalculatorOperationMultiply || operator == CalculatorOperationDevide) {
        if (self.currentOperation == CalculatorOperationPlus || self.currentOperation == CalculatorOperationMinus) {
            return true;
        }
    }
    return false;
}

- (NSString *)textToDisplay {
    if (!_textToDisplay) {
        _textToDisplay = @"0";
    }
    return _textToDisplay;
}

- (double)keepedNumber {
    if (!_keepedNumber) {
        _keepedNumber = 0;
    }
    return _keepedNumber;
}

- (double)leftNumber {
    if (!_leftNumber) {
        _leftNumber = 0;
    }
    return _leftNumber;
}

- (NSString *)rightNumber {
    if (!_rightNumber) {
        _rightNumber = @"0";
    }
    return _rightNumber;
}

@end
