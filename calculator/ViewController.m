//
//  ViewController.m
//  calculator
//
//  Created by Turtle Liu on 2020/4/6.
//  Copyright © 2020 Turtle Liu. All rights reserved.
//

#import "ViewController.h"
#import "CalculatorView.h"
#import "CalculatorViewModel.h"
#import <ReactiveObjC/ReactiveObjC.h>

@interface ViewController ()

@property (nonatomic) CalculatorViewModel * viewmodel;

@property (nonatomic) CalculatorView * calculatorView;

@end

@implementation ViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view = self.calculatorView;
    
    [self setUp];
    [self bindWithViewModel];
}

- (void)setUp {
    
    [self setUpNumberAction];
    [self setUpOperatorAction];
}

- (void)bindWithViewModel {
    
    [RACObserve(self.viewmodel, textToDisplay) subscribeNext:^(id  _Nullable x) {
        self.calculatorView.displayLabel.text = x;
    }];
}

- (void)setUpNumberAction {
    
    @weakify(self)
    [[self.calculatorView.zeroButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        [self.viewmodel enterNumber:@"0"];
        [self.calculatorView setButtonHighlight:NULL];
    }];
    
    [[self.calculatorView.oneButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterNumber:@"1"];
        [self.calculatorView setButtonHighlight:NULL];
    }];
    
    [[self.calculatorView.twoButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterNumber:@"2"];
        [self.calculatorView setButtonHighlight:NULL];
    }];
    
    [[self.calculatorView.threeButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterNumber:@"3"];
        [self.calculatorView setButtonHighlight:NULL];
    }];
    
    [[self.calculatorView.fourButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterNumber:@"4"];
        [self.calculatorView setButtonHighlight:NULL];
    }];
    
    [[self.calculatorView.fiveButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterNumber:@"5"];
        [self.calculatorView setButtonHighlight:NULL];
    }];
    
    [[self.calculatorView.sixButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterNumber:@"6"];
        [self.calculatorView setButtonHighlight:NULL];
    }];
    
    [[self.calculatorView.sevenButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterNumber:@"7"];
        [self.calculatorView setButtonHighlight:NULL];
    }];
    
    [[self.calculatorView.eightButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterNumber:@"8"];
        [self.calculatorView setButtonHighlight:NULL];
    }];
    
    [[self.calculatorView.nineButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterNumber:@"9"];
        [self.calculatorView setButtonHighlight:NULL];
    }];
    
    [[self.calculatorView.decimalButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterNumber:@"."];
        [self.calculatorView setButtonHighlight:NULL];
    }];
}

- (void)setUpOperatorAction {
    
    [[self.calculatorView.clearButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel clearAction];
        [self.calculatorView setButtonHighlight:NULL];
    }];
    
    [[self.calculatorView.negtiveButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterNumber:@"-"];
    }];
    
    [[self.calculatorView.percentButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        
    }];
    
    [[self.calculatorView.equalButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel equalAction];
        [self.calculatorView setButtonHighlight:NULL];
    }];
    
    [[self.calculatorView.plusButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterOperation:CalculatorOperationPlus];
    }];
    
    [[self.calculatorView.minusButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterOperation:CalculatorOperationMinus];
    }];
    
    [[self.calculatorView.multiplyButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterOperation:CalculatorOperationMultiply];
    }];
    
    [[self.calculatorView.devideButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.viewmodel enterOperation:CalculatorOperationDevide];
    }];
}

#pragma mark - Properties
- (CalculatorViewModel *)viewmodel {
    if (!_viewmodel) {
        _viewmodel = [[CalculatorViewModel alloc] init];
    }
    return _viewmodel;
}

- (CalculatorView *)calculatorView {
    if (!_calculatorView) {
        _calculatorView = [[CalculatorView alloc] init];
    }
    return _calculatorView;
}

@end
