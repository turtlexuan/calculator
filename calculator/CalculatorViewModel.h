//
//  CalculatorViewModel.h
//  calculator
//
//  Created by Turtle Liu on 2020/4/7.
//  Copyright © 2020 Turtle Liu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"

@interface CalculatorViewModel : NSObject

@property (strong, nonatomic) NSString *textToDisplay;

- (void)enterNumber:(NSString *)number;
- (void)enterOperation:(CalculatorOperation)operation;
- (void)equalAction;
- (void)clearAction;

@end
