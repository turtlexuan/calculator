//
//  CalculatorBaseButton.m
//  calculator
//
//  Created by Turtle Liu on 2020/4/7.
//  Copyright © 2020 Turtle Liu. All rights reserved.
//

#import "CalculatorBaseButton.h"

@interface CalculatorBaseButton()

@property (nonatomic, assign) float buttonWidth;

@end

@implementation CalculatorBaseButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        float screenWidth = UIScreen.mainScreen.bounds.size.width;
        self.buttonWidth = screenWidth / 4 - 20;
        
        self.layer.cornerRadius = self.buttonWidth / 2;
        self.layer.masksToBounds = true;
        self.titleLabel.font = [UIFont systemFontOfSize:30];
        UIColor* highlightedColor = [UIColor.whiteColor colorWithAlphaComponent:0.5];
        [self setBackgroundImage:[self imageWithColor:highlightedColor] forState:UIControlStateHighlighted];
        [self setBackgroundImage:[self imageWithColor:UIColor.whiteColor] forState:UIControlStateSelected];
        [self setTitleColor:UIColor.orangeColor forState:UIControlStateSelected];
    }
    return self;
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

@end
