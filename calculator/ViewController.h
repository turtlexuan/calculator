//
//  ViewController.h
//  calculator
//
//  Created by Turtle Liu on 2020/4/6.
//  Copyright © 2020 Turtle Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum calculatorNumber {
    CalculatorNumberZero = 1,
    CalculatorNumberOne,
    CalculatorNumberTwo,
    CalculatorNumberThree,
    CalculatorNumberFour,
    CalculatorNumberFive,
    CalculatorNumberSix,
    CalculatorNumberSeven,
    CalculatorNumberEight,
    CalculatorNumberNine,
    CalculatorNumberDecimal
} CalculatorNumber;

typedef enum calculatorOperation {
    CalculatorOperationNone = 1,
    CalculatorOperationEnd,
    CalculatorOperationDevide,
    CalculatorOperationMultiply,
    CalculatorOperationMinus,
    CalculatorOperationPlus
} CalculatorOperation;

@interface ViewController : UIViewController


@end

