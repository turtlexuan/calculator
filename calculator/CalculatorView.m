//
//  CalculatorView.m
//  calculator
//
//  Created by Turtle Liu on 2020/4/6.
//  Copyright © 2020 Turtle Liu. All rights reserved.
//

#import "CalculatorView.h"

@interface CalculatorView()

@property (nonatomic, assign) float buttonWidth;

@end

@implementation CalculatorView

- (instancetype)init {
    self = [super init];
    if (self) {
        
        float screenWidth = UIScreen.mainScreen.bounds.size.width;
        self.buttonWidth = screenWidth / 4 - 20;
        
        [self addSubviews];
        [self setConstraints];
    }
    return self;
}

- (void)addSubviews {
    [self addSubview: self.displayLabel];
    [self addSubview: self.clearButton];
    [self addSubview: self.negtiveButton];
    [self addSubview: self.percentButton];
    [self addSubview: self.equalButton];
    [self addSubview: self.plusButton];
    [self addSubview: self.minusButton];
    [self addSubview: self.multiplyButton];
    [self addSubview: self.devideButton];
    [self addSubview: self.decimalButton];
    [self addSubview: self.zeroButton];
    [self addSubview: self.oneButton];
    [self addSubview: self.twoButton];
    [self addSubview: self.threeButton];
    [self addSubview: self.fourButton];
    [self addSubview: self.fiveButton];
    [self addSubview: self.sixButton];
    [self addSubview: self.sevenButton];
    [self addSubview: self.eightButton];
    [self addSubview: self.nineButton];
}

- (void)setConstraints {
    
    UIView *superview = self;
    
    [self.zeroButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superview.mas_safeAreaLayoutGuideBottom).inset(30);
        make.leading.equalTo(superview).inset(10);
        make.height.mas_equalTo(self.buttonWidth);
        make.width.mas_equalTo(self.buttonWidth * 2 + 20);
    }];
    
    [self.decimalButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.zeroButton);
        make.leading.equalTo(self.zeroButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.equalButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.zeroButton);
        make.leading.equalTo(self.decimalButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.oneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.zeroButton.mas_top).offset(-20);
        make.leading.equalTo(superview).inset(10);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.twoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.oneButton);
        make.leading.equalTo(self.oneButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.threeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.oneButton);
        make.leading.equalTo(self.twoButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.plusButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.oneButton);
        make.leading.equalTo(self.threeButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.fourButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.oneButton.mas_top).offset(-20);
        make.leading.equalTo(superview).inset(10);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.fiveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.fourButton);
        make.leading.equalTo(self.fourButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];

    [self.sixButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.fourButton);
        make.leading.equalTo(self.fiveButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.minusButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.fourButton);
        make.leading.equalTo(self.sixButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.sevenButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.fourButton.mas_top).offset(-20);
        make.leading.equalTo(superview).inset(10);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.eightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.sevenButton);
        make.leading.equalTo(self.sevenButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.nineButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.sevenButton);
        make.leading.equalTo(self.eightButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.multiplyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.sevenButton);
        make.leading.equalTo(self.nineButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.clearButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.sevenButton.mas_top).offset(-20);
        make.leading.equalTo(superview).inset(10);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.negtiveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.clearButton);
        make.leading.equalTo(self.clearButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.percentButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.clearButton);
        make.leading.equalTo(self.negtiveButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.devideButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.clearButton);
        make.leading.equalTo(self.percentButton.mas_trailing).offset(20);
        make.height.width.mas_equalTo(self.buttonWidth);
    }];
    
    [self.displayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(superview).inset(10);
        make.bottom.equalTo(self.clearButton.mas_top).offset(-20);
        make.height.mas_equalTo(self.buttonWidth);
    }];
}

- (void)setButtonHighlight:(nullable UIButton *)button {
    self.plusButton.selected = false;
    self.minusButton.selected = false;
    self.multiplyButton.selected = false;
    self.devideButton.selected = false;
    button.selected = true;
}

#pragma mark - Properties
- (UILabel *)displayLabel {
    if (!_displayLabel) {
        _displayLabel = [[UILabel alloc] init];
        _displayLabel.text = @"0";
        _displayLabel.textColor = UIColor.whiteColor;
        _displayLabel.font = [UIFont systemFontOfSize:72];
        _displayLabel.textAlignment = NSTextAlignmentRight;
    }
    return _displayLabel;
}

- (CalculatorBaseButton *)clearButton {
    if (!_clearButton) {
        _clearButton = [[CalculatorBaseButton alloc] init];
        _clearButton.backgroundColor = UIColor.grayColor;
        [_clearButton setTitle:@"AC" forState:UIControlStateNormal];
        [_clearButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _clearButton;
}

- (CalculatorBaseButton *)negtiveButton {
    if (!_negtiveButton) {
        _negtiveButton = [[CalculatorBaseButton alloc] init];
        _negtiveButton.backgroundColor = UIColor.grayColor;
        [_negtiveButton setTitle:@"+/-" forState:UIControlStateNormal];
        [_negtiveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _negtiveButton;
}

- (CalculatorBaseButton *)percentButton {
    if (!_percentButton) {
        _percentButton = [[CalculatorBaseButton alloc] init];
        _percentButton.backgroundColor = UIColor.grayColor;
        [_percentButton setTitle:@"%" forState:UIControlStateNormal];
        [_percentButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _percentButton;
}

- (CalculatorBaseButton *)equalButton {
    if (!_equalButton) {
        _equalButton = [[CalculatorBaseButton alloc] init];
        _equalButton.backgroundColor = UIColor.orangeColor;
        [_equalButton setTitle:@"=" forState:UIControlStateNormal];
        [_equalButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _equalButton;
}

- (CalculatorBaseButton *)plusButton {
    if (!_plusButton) {
        _plusButton = [[CalculatorBaseButton alloc] init];
        _plusButton.backgroundColor = UIColor.orangeColor;
        [_plusButton setTitle:@"+" forState:UIControlStateNormal];
        [_plusButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _plusButton;
}

- (CalculatorBaseButton *)minusButton {
    if (!_minusButton) {
        _minusButton = [[CalculatorBaseButton alloc] init];
        _minusButton.backgroundColor = UIColor.orangeColor;
        [_minusButton setTitle:@"-" forState:UIControlStateNormal];
        [_minusButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _minusButton;
}

- (CalculatorBaseButton *)multiplyButton {
    if (!_multiplyButton) {
        _multiplyButton = [[CalculatorBaseButton alloc] init];
        _multiplyButton.backgroundColor = UIColor.orangeColor;
        [_multiplyButton setTitle:@"x" forState:UIControlStateNormal];
        [_multiplyButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _multiplyButton;
}

- (CalculatorBaseButton *)devideButton {
    if (!_devideButton) {
        _devideButton = [[CalculatorBaseButton alloc] init];
        _devideButton.backgroundColor = UIColor.orangeColor;
        [_devideButton setTitle:@"÷" forState:UIControlStateNormal];
        [_devideButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _devideButton;
}

- (CalculatorBaseButton *)decimalButton {
    if (!_decimalButton) {
        _decimalButton = [[CalculatorBaseButton alloc] init];
        _decimalButton.backgroundColor = UIColor.orangeColor;
        [_decimalButton setTitle:@"." forState:UIControlStateNormal];
        [_decimalButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _decimalButton;
}

- (CalculatorBaseButton *)zeroButton {
    if (!_zeroButton) {
        _zeroButton = [[CalculatorBaseButton alloc] init];
        _zeroButton.backgroundColor = UIColor.darkGrayColor;
        [_zeroButton setTitle:@"0" forState:UIControlStateNormal];
        [_zeroButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _zeroButton;
}

- (CalculatorBaseButton *)oneButton {
    if (!_oneButton) {
        _oneButton = [[CalculatorBaseButton alloc] init];
        _oneButton.backgroundColor = UIColor.darkGrayColor;
        [_oneButton setTitle:@"1" forState:UIControlStateNormal];
        [_oneButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _oneButton;
}

- (CalculatorBaseButton *)twoButton {
    if (!_twoButton) {
        _twoButton = [[CalculatorBaseButton alloc] init];
        _twoButton.backgroundColor = UIColor.darkGrayColor;
        [_twoButton setTitle:@"2" forState:UIControlStateNormal];
        [_twoButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _twoButton;
}

- (CalculatorBaseButton *)threeButton {
    if (!_threeButton) {
        _threeButton = [[CalculatorBaseButton alloc] init];
        _threeButton.backgroundColor = UIColor.darkGrayColor;
        [_threeButton setTitle:@"3" forState:UIControlStateNormal];
        [_threeButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _threeButton;
}

- (CalculatorBaseButton *)fourButton {
    if (!_fourButton) {
        _fourButton = [[CalculatorBaseButton alloc] init];
        _fourButton.backgroundColor = UIColor.darkGrayColor;
        [_fourButton setTitle:@"4" forState:UIControlStateNormal];
        [_fourButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _fourButton;
}

- (CalculatorBaseButton *)fiveButton {
    if (!_fiveButton) {
        _fiveButton = [[CalculatorBaseButton alloc] init];
        _fiveButton.backgroundColor = UIColor.darkGrayColor;
        [_fiveButton setTitle:@"5" forState:UIControlStateNormal];
        [_fiveButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _fiveButton;
}

- (CalculatorBaseButton *)sixButton {
    if (!_sixButton) {
        _sixButton = [[CalculatorBaseButton alloc] init];
        _sixButton.backgroundColor = UIColor.darkGrayColor;
        [_sixButton setTitle:@"6" forState:UIControlStateNormal];
        [_sixButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _sixButton;
}

- (CalculatorBaseButton *)sevenButton {
    if (!_sevenButton) {
        _sevenButton = [[CalculatorBaseButton alloc] init];
        _sevenButton.backgroundColor = UIColor.darkGrayColor;
        [_sevenButton setTitle:@"7" forState:UIControlStateNormal];
        [_sevenButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _sevenButton;
}

- (CalculatorBaseButton *)eightButton {
    if (!_eightButton) {
        _eightButton = [[CalculatorBaseButton alloc] init];
        _eightButton.backgroundColor = UIColor.darkGrayColor;
        [_eightButton setTitle:@"8" forState:UIControlStateNormal];
        [_eightButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _eightButton;
}

- (CalculatorBaseButton *)nineButton {
    if (!_nineButton) {
        _nineButton = [[CalculatorBaseButton alloc] init];
        _nineButton.backgroundColor = UIColor.darkGrayColor;
        [_nineButton setTitle:@"9" forState:UIControlStateNormal];
        [_nineButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _nineButton;
}

@end
