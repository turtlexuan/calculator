//
//  SceneDelegate.h
//  calculator
//
//  Created by Turtle Liu on 2020/4/6.
//  Copyright © 2020 Turtle Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

